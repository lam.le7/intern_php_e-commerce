<?php

namespace App\Http\Livewire\Admin;

use App\Models\HomeSlider;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class AdminEditHomeSliderComponent extends Component
{
    use WithFileUploads;
    public $title;
    public $subtile;
    public $price;
    public $link;
    public $image;
    public $newimage;
    public $status;
    public $slider_id;

    public function mount($slider_id)
    {
        $slider = HomeSlider::find($slider_id);
        $this->title = $slider -> title;
        $this->subtile = $slider -> subtile;
        $this->price = $slider -> price;
        $this->link = $slider -> link;
        $this->image = $slider -> image;
        $this->status = $slider -> status;
        $this-> slider_id = $slider -> id;
    }

    public function editSlider()
    {
        $slider = HomeSlider::find($this -> slider_id);
        $slider -> title = $this -> title;
        $slider -> subtile = $this -> subtile;
        $slider -> price = $this -> price;
        $slider -> link = $this -> link;
        if($this -> newimage){
            $imageName = Carbon::now() -> timestamp. '.' . $this -> newimage -> extension();
            $this -> newimage->storeAs('sliders', $imageName);
            $slider -> image = $imageName;
        }
        $slider -> status = $this -> status;
        $slider -> save();
        session() -> flash('message', 'Slider has been updated successfully');
    }

    public function render()
    {
        return view('livewire.admin.admin-edit-home-slider-component')->layout('layouts.base');
    }
}
