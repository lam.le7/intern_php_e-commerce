<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->utype === "ADM"){
            #if(session('utype' === 'ADM')){
                return $next($request);
            }
            else{
                session()->flush(); //Yêu cầu máy chủ gửi đầu ra hiện đang được đệm đến trình duyệt.
                return redirect()->route('login');
            }
            return $next($request);
    }
}
